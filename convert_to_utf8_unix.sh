#!/bin/bash

# encodage voulu
encodeUtf8='utf-8'

# Conversion ?
CONV=false

# Check path
if [ ! -d "$1" ]; then
	echo "Path <$1> inconnu"
	exit 1
fi

# Check options
if [[ "-r" == "$2" ]]; then
	CONV=false
elif [[ "-w" == "$2" ]]; then
	CONV=true
fi


for filename in ` find $1 -type f -iname *.c -o -iname *.h`
do
	encodeFrom=$(file -b --mime-encoding $filename)

	echo "Fichier ${filename} Encodage :" ${encodeFrom}

	if ${CONV}; then
		if [[ "utf-8" != ${encodeFrom} ]]; then 
			echo "Fichier ${filename} a mettre à jour"
			
			# sauvegarde du fichier source
			mv $filename $filename.save
			# ecriture du fichier encode
			iconv -f $encodeFrom -t $encodeUtf8 $filename.save -o $filename
			rm -rf $filename.save
			
		fi
		dos2unix -q $filename
	fi
done




